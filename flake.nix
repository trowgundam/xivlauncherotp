{
  inputs = {
    naersk = {
      url = "github:nix-community/naersk/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        lib = pkgs.lib;
      in
      {
        packages.default = naersk-lib.buildPackage rec {
          src = ./.;
          nativeBuildInputs = with pkgs; [ pkg-config copyDesktopItems ];
          buildInputs = with pkgs; [ openssl ];

          postInstall = ''
            mkdir -p $out/share/pixmaps
            cp resources/512.png $out/share/pixmaps/xivlauncher_otp.png
          '';

          executables = [ "xivlauncher_otp" ];
          runtimeDeps = [ pkgs.libsecret ];
          desktopItems = [
            (pkgs.makeDesktopItem {
              name = "xivlauncher_otp";
              exec = "xivlauncher_otp xiv-launcher";
              icon = "xivlauncher_otp";
              desktopName = "XIVLauncher OTP";
              comment = meta.description;
              categories = [ "Game" ];
            })
          ];
          meta = with lib; {
            description = "A tool for saving 2FA codes and sending them to XIVLauncher";
            homepage = "https://gitlab.com/trowgundam/xivlauncherotp";
            license = licenses.mit;
            mainProgram = "xivlauncher_otp";
          };
        };
        devShells.default = with pkgs; mkShell {
          buildInputs = [ cargo rustc rustfmt pre-commit rustPackages.clippy ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });
}
