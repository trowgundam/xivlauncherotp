extern crate clap;
extern crate keyring;
extern crate reqwest;
extern crate serde;
extern crate serde_json;

//==================================================================================================
// Start of use statements
// stdlib
use std::io::stdin;
use std::time::{SystemTime, UNIX_EPOCH};

// crates
use base32::Alphabet;
use clap::Parser;
use cli::{Cli, Commands};
use totp_lite::{totp_custom, Sha1, Sha256, Sha512};

use crate::algorithm::Algorithm;
use crate::token::TotpToken;

mod algorithm;
mod cli;
mod token;
//==================================================================================================

//==================================================================================================
// Application Constants
const SERVICE_NAME: &str = "net.trowgundam.xivlauncherotp";
//==================================================================================================

fn main() {
    let cli = Cli::parse();

    match cli.command {
        Commands::Setup {
            name,
            key,
            algorithm,
            digits,
            period,
        } => {
            let name = name.unwrap_or("Default".to_string());

            let entry = keyring::Entry::new(SERVICE_NAME, name.as_str());
            if let Err(er) = entry {
                eprintln!("Failed to Retrieve Secret: {er}");
                std::process::exit(-1);
            }
            let entry = entry.unwrap();
            if entry.get_password().is_ok() {
                print!("A token named \"{name}\" already exists. Replace? (y/N): ");

                let mut response = String::new();
                match stdin().read_line(&mut response) {
                    Ok(_) => match response.to_uppercase().as_str() {
                        "YES" | "Y" => {
                            if let Err(e) = entry.delete_credential() {
                                eprintln!("{}", e);
                                std::process::exit(-1);
                            };
                        }
                        "NO" | "N" => {
                            println!("Aborting Setup");
                            std::process::exit(0);
                        }
                        _ => {
                            eprintln!("Invalid response: {response}");
                            std::process::exit(-1);
                        }
                    },
                    Err(e) => {
                        eprintln!("Failed to read input: {}", e);
                        std::process::exit(-1);
                    }
                }
            };

            let token = TotpToken {
                key,
                algorithm,
                digits,
                period,
            };
            let json = match serde_json::to_string(&token) {
                Ok(e) => e,
                Err(e) => {
                    eprintln!("Failed to Serialize Token: {}", e);
                    std::process::exit(-1);
                }
            };
            match entry.set_password(json.as_str()) {
                Ok(_) => {
                    println!("Token {name} Saved");
                    std::process::exit(0);
                }
                Err(e) => {
                    eprintln!("Failed to save Token: {}", e);
                }
            }
        }
        Commands::Remove { name } => {
            let name = name.unwrap_or("Default".to_string());

            let entry = keyring::Entry::new(SERVICE_NAME, name.as_str());
            if let Err(er) = entry {
                eprintln!("Failed to Retrieve Secret: {er}");
                std::process::exit(-1);
            }
            let entry = entry.unwrap();
            if let Err(e) = entry.delete_credential() {
                eprintln!("Failed to delete token from Secrets Store: {e}");
                std::process::exit(-1);
            };
        }
        Commands::Generate { name, quiet } => {
            let name = name.unwrap_or("Default".to_string());

            match get_token(name.as_str()) {
                Ok(t) => {
                    if quiet {
                        print!("{}", t);
                    } else {
                        println!("Token: {}", t);
                    }

                    std::process::exit(0);
                }
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(-1);
                }
            };
        }
        Commands::XivLauncher {
            name,
            computer_name,
        } => {
            let name = name.unwrap_or("Default".to_string());
            let computer_name = computer_name.unwrap_or("127.0.0.1".to_string());

            let token = match get_token(name.as_str()) {
                Ok(t) => t,
                Err(e) => {
                    eprintln!("{}", e);
                    std::process::exit(-1);
                }
            };

            let url = format!("http://{}:4646/ffxivlauncher/{}", computer_name, token);
            match reqwest::blocking::get(url) {
                Ok(_) => {
                    std::process::exit(0);
                }
                Err(e) => {
                    eprintln!("Failed XIVLauncher request: {}", e);
                    std::process::exit(-1);
                }
            };
        }
    };
}

fn get_token(token_name: &str) -> Result<String, String> {
    // Get token JSON from Secrets Store
    let entry = keyring::Entry::new(SERVICE_NAME, token_name);
    if let Err(er) = entry {
        return Err(format!("Failed to Retrieve Secret: {er}"));
    }
    let entry = entry.unwrap();
    match entry.get_password() {
        Ok(t) => {
            // Deserialize token JSON
            let token: TotpToken = match serde_json::from_str(t.as_str()) {
                Ok(to) => to,
                Err(e) => {
                    return Err(format!("Failed to deserialize token: {}", e));
                }
            };

            // Start computation of TOTP Token
            let mut key: Vec<u8> = vec![];
            match base32::decode(Alphabet::Rfc4648 { padding: false }, token.key.as_str()) {
                None => {
                    return Err("Failed to decode secret.".to_string());
                }
                Some(b) => {
                    key.clone_from(&b);
                }
            };
            let key = key.as_slice();

            let seconds = match SystemTime::now().duration_since(UNIX_EPOCH) {
                Ok(s) => s.as_secs(),
                Err(e) => {
                    return Err(format!("Failed to get time: {}", e));
                }
            };
            match token.algorithm {
                Algorithm::Sha1 => Ok(totp_custom::<Sha1>(
                    token.period,
                    token.digits,
                    key,
                    seconds,
                )),
                Algorithm::Sha256 => Ok(totp_custom::<Sha256>(
                    token.period,
                    token.digits,
                    key,
                    seconds,
                )),
                Algorithm::Sha512 => Ok(totp_custom::<Sha512>(
                    token.period,
                    token.digits,
                    key,
                    seconds,
                )),
            }
        }
        Err(e) => Err(format!("Failed to retrieve token: {}", e)),
    }
}
