# XIVLauncher OTP

This tool is meant as a simple application to store and fill the OTP token for the XIVLauncher application.

## Supported Platforms
- Windows  
  Should work straight out of the box, nothing extra.
- Linux  
  Requires a package that implements ```org.freedesktop.secrets```. Something like ```gnome-keyring``` works.
- MacOS (Theoretically)

See [keyring](https://docs.rs/keyring/1.1.2/keyring/) for more information on secret stores by platform.

## Commands
### Setup
```setup```  
This command is used to add a token to the Secret store. Requires a

#### Optional Parameters
- ```-a```, ```--algorithm``` ```<Algorithm>```  
  Used to specify the algorithm used to generate the TOTP Token. Possible values:
  - SHA1
  - SHA256
  - SHA512
  
  Will default to SHA1, which is the standard for most sites.
- ```-d```, ```--digits``` ```<Digits>```  
  Number of digits included in the TOTP Token. Defaults to 6, which is the most common value used.
- ```-n```, ```--name``` ```<Name>```  
  Specifies a name for the TOTP Token. Used for storing and referring to the token in other commands. This is only
  required if storing more than one TOTP Token.
- ```-p```, ```--period``` ```<Period>```  
  The amount of time in seconds that a token is good form. Defaults to 30, the most common value.

### Remove
```remove```
This is used to remove a TOTP Token from your Secrets store.

#### Optional Parameters
- ```-n```, ```--name``` ```<Name>```  
  Specifies the name of the token to remove. If not specified, the default token will be removed from your Secrets
  store.

### Generate
```generate```
This is used to generate a TOTP Token.

#### Optional Parameters
- ```-n```, ```--name``` ```<Name>```  
  Specifies the name of the token to generate a TOTP Token for. If not specified, the default token will be generated.
- ```-q```, ```--quiet```
  Doesn't output extra text or a new line character. Useful for scripting.

### XIVLauncher
```xiv-launcher```
This is used to generate a TOTP Token and send it to XIVLauncher.

#### Optional Parameters
- ```-c```, ```--computer-name``` ```<ComputerName>```  
  The name or IP of the computer to send the TOTP Token to. Defaults to "127.0.0.1", the localhost.
- ```-n```, ```--name``` ```<Name>```  
  Specifies the name of the token to generate a TOTP Token for. If not specified, the default token will be generated.

## How it works
First you need to set up a TOTP Token using the ```setup``` command. You only need to use the ```--name``` optional
parameter for this command, and others, if you are storing multiple TOTP tokens. While this is intended for use with
XIVLauncher. It can work for any site that uses the standard TOTP algorithm.

All TOTP Token information is stored in your Platform specific Secrets store. For Windows, this is the Credential
Manager. On Linux it uses a package that implements ```org.freedesktop.secrets```, such as ```gnome-keyring```.
Theoretically, it should also work on macOS with the Secure Keychain, but I am unable to test this due to not owning a
macOS device.

Once a token has been stored in your Secrets store, you can use the ```generate``` command to generate the token, which
will be printed to the command line, or the ```xiv-launcher``` to generate the token and pass it to XIVLauncher. This
requires you to enable the "Enable XL Authenticator app/OTP macro support" in XIVLauncher. You can even send it to
another device using the ```--computer-name``` optional parameter and passing the Computer Name or IP (preferred).

## Credits
- Credits to [XIVLauncher](https://github.com/goatcorp/FFXIVQuickLauncher) for their wonderful 3rd Party Launcher.
- Icon is just a very, very basic combination of the XIVLauncher icon and an icon found on
  [flaticon.com](https://flaticon.com)  
  [Token icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/token)
- Crates used
  - [keyring](https://crates.io/crates/keyring)
  - [clap](https://crates.io/crates/clap)
  - [totp-lite](https://crates.io/crates/totp-lite)
  - [base32](https://crates.io/crates/base32)
  - [serde](https://crates.io/crates/serde)
  - [serde_json](https://crates.io/crates/serde_json)
  - [reqwest](https://crates.io/crates/reqwest)
  - [winres](https://crates.io/crates/winres)
