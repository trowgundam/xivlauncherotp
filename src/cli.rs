use crate::algorithm::Algorithm;
use clap::{Parser, Subcommand};

#[derive(Parser)]
#[clap(name = "XIVLauncher OTP", author, version, about, long_about = None)]
#[clap(propagate_version = true)]
pub struct Cli {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    #[clap(about = "Add TOTP Token to Secrets Store")]
    Setup {
        #[clap(short, long, help = "Token Name, if blank will setup default token.")]
        name: Option<String>,

        #[clap(help = "Secret key or URI for TOTP Token")]
        key: String,

        #[clap(value_enum, short, long, default_value_t = Algorithm::Sha1, help = "Algorithm used to generate token")]
        algorithm: Algorithm,

        #[clap(short, long, default_value_t = 6, help = "Number of digits in token")]
        digits: u32,

        #[clap(
            short,
            long,
            default_value_t = 30,
            help = "How long a token is valid in seconds"
        )]
        period: u64,
    },

    #[clap(about = "Remove a TOTP Token from Secrets Store")]
    Remove {
        #[clap(short, long, help = "Token Name, if blank will remove default token.")]
        name: Option<String>,
    },

    #[clap(about = "Generates TOTP Token")]
    Generate {
        #[clap(
            short,
            long,
            help = "Token Name, if blank will generate default token."
        )]
        name: Option<String>,

        #[clap(
            short,
            long,
            help = "Only output TOTP token, without label or new line. Useful for scripting"
        )]
        quiet: bool,
    },

    #[clap(about = "Sends TOTP Token to XIVLauncher")]
    XivLauncher {
        #[clap(
            short,
            long,
            help = "Token Name, if blank will generate default token."
        )]
        name: Option<String>,

        #[clap(
            short,
            long,
            help = "Computer Name to Send TOTP Token To, defaults to 127.0.0.1"
        )]
        computer_name: Option<String>,
    },
}
