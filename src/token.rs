use crate::algorithm::Algorithm;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct TotpToken {
    pub key: String,
    pub algorithm: Algorithm,
    pub digits: u32,
    pub period: u64,
}
